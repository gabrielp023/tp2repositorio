package Clases;

public class Celda {
	
	Personaje personaje;
	boolean estaLibre;
	
	public Celda() {
		this.estaLibre=true;
		this.personaje=null;
	}
	
	public boolean puedoColocarPersonaje(){
		return estaLibre;
	}

	public void agregarPersonaje(Personaje personaje) {
		this.estaLibre=false;
		this.personaje=personaje;
	}

	public boolean elPersonajeEstaEnLaCelda(Personaje personaje) {
		if(personaje == this.personaje){
			return true;
		}
		else{
			return false;
		}
	}
	
	public void sacarPersonaje() {
			this.personaje=null;
			this.estaLibre=true;			
	}
}
