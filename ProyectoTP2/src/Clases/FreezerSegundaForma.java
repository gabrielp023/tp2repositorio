package Clases;

public class FreezerSegundaForma extends Personaje{
	
	public FreezerSegundaForma(){
		super("Freezer Segunda Forma",400,40,3,4,0,20,false,new FreezerDefinitivo());
	}

	public boolean chequearSiEvoluciona(){
		return true;
	}
}
