package Clases;

public class Juego {
	Tablero tablero;
	JugadorGuerreroZ jugadorGuerreroZ;
	JugadorEnemigoDeLaTierra jugadorEnemigoDeLaTierra;
	
	
	public Juego(){
		this.tablero = new Tablero();
		this.jugadorGuerreroZ = new JugadorGuerreroZ();
		this.jugadorEnemigoDeLaTierra = new JugadorEnemigoDeLaTierra();
		this.jugadorGuerreroZ.UbicarPersonajes(tablero);
		this.jugadorEnemigoDeLaTierra.UbicarPersonajes(tablero);
	}
	
	
	
	//Metodos para correr tests
	
	public int obtenerColumnaDePersonaje1JugadorEnemigoDeLaTierra(){
		return this.tablero.obtenerColumnaDe(jugadorEnemigoDeLaTierra.personaje1);
	}
	public int obtenerFilaDePersonaje1JugadorEnemigoDeLaTierra(){
		return this.tablero.obtenerFilaDe(jugadorEnemigoDeLaTierra.personaje1);
	}
	public int obtenerColumnaDePersonaje2JugadorEnemigoDeLaTierra(){
		return this.tablero.obtenerColumnaDe(jugadorEnemigoDeLaTierra.personaje2);
	}
	public int obtenerFilaDePersonaje2JugadorEnemigoDeLaTierra(){
		return this.tablero.obtenerFilaDe(jugadorEnemigoDeLaTierra.personaje2);
	}
	public int obtenerColumnaDePersonaje3JugadorEnemigoDeLaTierra(){
		return this.tablero.obtenerColumnaDe(jugadorEnemigoDeLaTierra.personaje3);
	}
	public int obtenerFilaDePersonaje3JugadorEnemigoDeLaTierra(){
		return this.tablero.obtenerFilaDe(jugadorEnemigoDeLaTierra.personaje3);
	}

	public int obtenerColumnaDePersonaje1JugadorGuerreroZ(){
		return this.tablero.obtenerColumnaDe(jugadorGuerreroZ.personaje1);
	}
	public int obtenerFilaDePersonaje1JugadorGuerreroZ(){
		return this.tablero.obtenerFilaDe(jugadorGuerreroZ.personaje1);
	}
	public int obtenerColumnaDePersonaje2JugadorGuerreroZ(){
		return this.tablero.obtenerColumnaDe(jugadorGuerreroZ.personaje2);
	}
	public int obtenerFilaDePersonaje2JugadorGuerreroZ(){
		return this.tablero.obtenerFilaDe(jugadorGuerreroZ.personaje2);
	}
	public int obtenerColumnaDePersonaje3JugadorGuerreroZ(){
		return this.tablero.obtenerColumnaDe(jugadorGuerreroZ.personaje3);
	}
	public int obtenerFilaDePersonaje3JugadorGuerreroZ(){
		return this.tablero.obtenerFilaDe(jugadorGuerreroZ.personaje3);
	}


}
