package Clases;

public abstract class Jugador {
	Personaje personaje1;
	Personaje personaje2;
	Personaje personaje3;


	public Jugador(Personaje personaje1, Personaje personaje2, Personaje personaje3){
		this.personaje1 = personaje1;
		this.personaje2 = personaje2;
		this.personaje3 = personaje3;
	}
}