package Clases;

public class JugadorEnemigoDeLaTierra extends Jugador{
	public JugadorEnemigoDeLaTierra(){
		super(new Freezer(), new Cell(), new MajinBoo());
		}
	public void UbicarPersonajes(Tablero tablero){
		tablero.ubicarEnemigoEnLaTierra(personaje1);
		tablero.ubicarEnemigoEnLaTierra(personaje2);
		tablero.ubicarEnemigoEnLaTierra(personaje3);
	}
}
