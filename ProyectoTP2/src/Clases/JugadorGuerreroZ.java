package Clases;

public class JugadorGuerreroZ extends Jugador{
	public JugadorGuerreroZ(){
		super(new Goku(), new Gohan(), new Piccolo());
		}
	public void UbicarPersonajes(Tablero tablero){
		tablero.ubicarGuerreroZ(personaje1);
		tablero.ubicarGuerreroZ(personaje2);
		tablero.ubicarGuerreroZ(personaje3);
	}
}
