package Clases;

import Excepciones.ExcepcionNoPuedeEvolucionar;
import Excepciones.ExcepcionNoSePuedeRealizarAtaque;


public abstract class Personaje {
	String nombre;
	int puntosDeVida;
	int poderDePelea;
	int distanciaDeAtaque;
	int velocidadDeDesplazamiento;
	int ki;
	int kiNecesarioParaIniciar;
	boolean tieneRequerimientoEspecial;
	Personaje ProximaEvolucion;
	
	public Personaje(){
		
	}
	
	public Personaje(String nombre,int Vida,int poder,int distancia,int velocidad,int ki,int kiNecesario,boolean requiereAlgoAdicional,Personaje evolucion){
		this.nombre = nombre;
		this.puntosDeVida=Vida;
		this.poderDePelea=poder;
		this.distanciaDeAtaque=distancia;
		this.velocidadDeDesplazamiento=velocidad;
		this.ki=ki;
		this.kiNecesarioParaIniciar=kiNecesario;
		this.tieneRequerimientoEspecial=requiereAlgoAdicional;	
		this.ProximaEvolucion=evolucion;
	}
		
	public String obtenerNombre(){return this.nombre;}
	
	public int poderDePelea(){return this.poderDePelea;}
	
	public int obtenerDistanciaDeAtaque(){return this.distanciaDeAtaque;}
	
	public int obtenerVelocidad(){return this.velocidadDeDesplazamiento;}
	
	public int	kiNecesario(){return this.kiNecesarioParaIniciar;}
	
	public int	puntosDeVida(){return this.puntosDeVida;}
	
	public boolean requerimientoEspecial(){	return this.tieneRequerimientoEspecial;}
	
	public Personaje obtenerEvolucion(){return this.ProximaEvolucion;}

	
	public void agregarKi(int numero){
		this.ki=this.ki+numero;
	}
	
	public int ki(){
		return this.ki;
	}
	
	private int kiMinimoParaInvocar(){
		return this.kiNecesarioParaIniciar;
	}

	public boolean puedeTransformar(){
		boolean puede;
		if(this.ProximaEvolucion==null){return false;}
		boolean tieneSuficienteKi=this.tieneKiSuficienteParaTransformar();
		if(tieneSuficienteKi){
			if(!this.ProximaEvolucion.tieneRequerimientoEspecial){puede=true;}else{puede=this.ProximaEvolucion.chequearCondicionDeEvolucion();}
			}
		else{puede=false;}
		return puede;
	}
	
	private boolean tieneKiSuficienteParaTransformar(){
		boolean tieneKi=(this.ki>=this.ProximaEvolucion.kiMinimoParaInvocar());
		return tieneKi;
	}
	
	private boolean chequearCondicionDeEvolucion(){
		return this.chequearSiEvoluciona();
	}
	
	public void transformar(){
		if (!this.puedeTransformar()){
			throw new ExcepcionNoPuedeEvolucionar();
		}
		else{
			this.nombre=this.ProximaEvolucion.nombre;
			this.poderDePelea=this.ProximaEvolucion.poderDePelea;
			this.distanciaDeAtaque=this.ProximaEvolucion.distanciaDeAtaque;
			this.velocidadDeDesplazamiento=this.ProximaEvolucion.velocidadDeDesplazamiento;
			this.kiNecesarioParaIniciar=this.ProximaEvolucion.kiNecesarioParaIniciar;
			this.tieneRequerimientoEspecial=this.ProximaEvolucion.tieneRequerimientoEspecial;
			this.ProximaEvolucion=this.ProximaEvolucion.ProximaEvolucion;
		}
	}

	public boolean puedeAtacarA(Personaje victima, Tablero tablero){
		
		return ((Math.abs((tablero.obtenerColumnaDe(victima)-tablero.obtenerColumnaDe(this)))<=2)&&(Math.abs((tablero.obtenerFilaDe(victima)-tablero.obtenerFilaDe(this)))<=2));
	}
	
	public void atacaA(Personaje victima, Tablero tablero){
		if (!this.puedeAtacarA(victima, tablero)){
			throw new ExcepcionNoSePuedeRealizarAtaque();
		}
		else{
			victima.puntosDeVida = (victima.puntosDeVida - this.poderDePelea);
	};
	}
	
	
	public abstract boolean chequearSiEvoluciona();



}
