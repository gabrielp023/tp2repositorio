package Clases;

public class PiccoloFortalecido extends Personaje {

	public PiccoloFortalecido() {
		super("Piccolo Fortalecido",500,40,4,3,0,20,false,new PiccoloProtector());
	}

	public boolean chequearSiEvoluciona(){
		return true;
	}
}
