package Clases;

import Excepciones.ExcepcionNoSePuedeRealizarElMovimiento;

public class Tablero {
	
	int Columnas=10;
	int Filas=10;
	Celda[][] campoDeBatalla;
	
	//Inicializa tablero en 0
	public Tablero(){
			this.campoDeBatalla = new Celda[Filas][Columnas];
			for (int fila=0;fila < Filas ;fila++) {
				for (int columna=0;columna < Columnas ;columna++) {
					campoDeBatalla[fila][columna]= new Celda();
				}
			}
	}
	
	public void ubicarGuerreroZ(Personaje personaje){
		for (int columna=0;columna < 3;columna++) {
			if (campoDeBatalla[0][columna].puedoColocarPersonaje()){
				campoDeBatalla[0][columna].agregarPersonaje(personaje);
				break;
			}			 
		}
	}
	public void ubicarEnemigoEnLaTierra(Personaje personaje){
		for (int columna=7;columna < 10;columna++) {
			if (campoDeBatalla[9][columna].puedoColocarPersonaje()){
				campoDeBatalla[9][columna].agregarPersonaje(personaje);
				break;
			}			 
		}
	}
	public boolean mover(Personaje personaje,String movimiento){
		for (int fila=0; fila < Filas; fila++){
			for (int columna=0; columna < Columnas; columna++){
				if(campoDeBatalla[fila][columna].elPersonajeEstaEnLaCelda(personaje)){//busca el personaje		
						if((movimiento=="Izquierda") && ((columna-personaje.velocidadDeDesplazamiento)>=0) && (campoDeBatalla[fila][columna-personaje.velocidadDeDesplazamiento].estaLibre)){					
							campoDeBatalla[fila][columna].sacarPersonaje();
							campoDeBatalla[fila][columna-personaje.velocidadDeDesplazamiento].agregarPersonaje(personaje);
							return true;
						}
						if((movimiento=="Derecha") && ((columna+personaje.velocidadDeDesplazamiento)<Columnas)&& (campoDeBatalla[fila][columna+personaje.velocidadDeDesplazamiento].estaLibre)){
							campoDeBatalla[fila][columna].sacarPersonaje();
							campoDeBatalla[fila][columna+personaje.velocidadDeDesplazamiento].agregarPersonaje(personaje);
							return true;
						}
						if((movimiento=="Arriba") && ((fila-personaje.velocidadDeDesplazamiento)>=0) && (campoDeBatalla[fila-personaje.velocidadDeDesplazamiento][columna].estaLibre)){
							campoDeBatalla[fila][columna].sacarPersonaje();
							campoDeBatalla[fila-personaje.velocidadDeDesplazamiento][columna].agregarPersonaje(personaje);
							return true;
						}
						if((movimiento=="Abajo") && ((fila+personaje.velocidadDeDesplazamiento)<Filas) && (campoDeBatalla[fila+personaje.velocidadDeDesplazamiento][columna].estaLibre)){
							campoDeBatalla[fila][columna].sacarPersonaje();
							campoDeBatalla[fila+personaje.velocidadDeDesplazamiento][columna].agregarPersonaje(personaje);
							return true;
						}
				}//verificacion si el personaje esta en la celda
			}//ciclo columna
		}//ciclo fila
		throw new ExcepcionNoSePuedeRealizarElMovimiento();
	}

	public void ubicarEnTablero(int fila,int col,Personaje personaje){
		Celda celda=this.campoDeBatalla[fila][col];
		celda.agregarPersonaje(personaje);
	}

	
//Metodos para correr tests
	public int obtenerFilaDe(Personaje personaje){
		for (int fila=0;fila < Filas ;fila++) {
			for (int columna=0;columna < Columnas ;columna++) {
				if(campoDeBatalla[fila][columna].elPersonajeEstaEnLaCelda(personaje)){
					return fila;
				}
			}
		}
		return -1;
	}
	public int obtenerColumnaDe(Personaje personaje){
		for (int fila=0;fila < Filas ;fila++) {
			for (int columna=0;columna < Columnas ;columna++) {
				if(campoDeBatalla[fila][columna].elPersonajeEstaEnLaCelda(personaje)){
					return columna;
				}
			}
		}
		return -1;
	}
	

}

