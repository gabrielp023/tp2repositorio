package Test;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import Clases.Freezer;
import Clases.Gohan;
import Clases.Goku;
import Clases.Juego;
import Clases.JugadorGuerreroZ;
import Clases.Piccolo;
import Clases.Tablero;
import Excepciones.ExcepcionNoSePuedeRealizarElMovimiento;
import Excepciones.ExcepcionNoPuedeEvolucionar;
import Excepciones.ExcepcionNoSePuedeRealizarAtaque;

public class Integracion {
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();  
	
	@Test
	public void test01CrearTableroYpersonajeParamover(){
		Tablero tablero = new Tablero();
		Goku goku = new Goku();
		tablero.ubicarGuerreroZ(goku);
		
		assertEquals(0, tablero.obtenerColumnaDe(goku));
		assertEquals(0, tablero.obtenerFilaDe(goku));
		
		thrown.expect(ExcepcionNoSePuedeRealizarElMovimiento.class);
		tablero.mover(goku, "Arriba");
		
		thrown.expect(ExcepcionNoSePuedeRealizarElMovimiento.class);
		tablero.mover(goku, "Izquierda");
		
		tablero.mover(goku, "Abajo");
		assertEquals(0, tablero.obtenerColumnaDe(goku));
		assertEquals(2, tablero.obtenerFilaDe(goku));
		
		tablero.mover(goku, "Abajo");
		assertEquals(0, tablero.obtenerColumnaDe(goku));
		assertEquals(4, tablero.obtenerFilaDe(goku));
		
		tablero.mover(goku,"Derecha");
		assertEquals(2, tablero.obtenerColumnaDe(goku));
		assertEquals(4, tablero.obtenerFilaDe(goku)); 
		
		tablero.mover(goku, "Izquierda");
		assertEquals(0, tablero.obtenerColumnaDe(goku));
		assertEquals(4, tablero.obtenerFilaDe(goku)); 
		
		tablero.mover(goku, "Arriba");
		assertEquals(0, tablero.obtenerColumnaDe(goku));
		assertEquals(2, tablero.obtenerFilaDe(goku)); 
	}
	
	@Test
	public void test02TresPersonajesEnElMismoCasillero(){
		Tablero tablero = new Tablero();
		Goku goku = new Goku();
		Gohan gohan = new Gohan();
		Piccolo piccolo = new Piccolo();
		tablero.ubicarGuerreroZ(goku);
		tablero.ubicarGuerreroZ(gohan);
		tablero.ubicarGuerreroZ(piccolo);
		
		assertEquals(0, tablero.obtenerColumnaDe(goku));
		assertEquals(0, tablero.obtenerFilaDe(goku));
		assertEquals(1, tablero.obtenerColumnaDe(gohan));
		assertEquals(0, tablero.obtenerFilaDe(gohan));
		assertEquals(2, tablero.obtenerColumnaDe(piccolo));
		assertEquals(0, tablero.obtenerFilaDe(piccolo));
		
		thrown.expect(ExcepcionNoSePuedeRealizarElMovimiento.class);
		tablero.mover(goku,"Derecha");
		
	}
	
	@Test
	public void test03BuscaPersonajeYSiPuedeLoTransforma(){
		
		Tablero tablero = new Tablero();
		Goku goku = new Goku();
		Gohan gohan = new Gohan();
		Piccolo piccolo = new Piccolo();
		tablero.ubicarGuerreroZ(goku);
		tablero.ubicarGuerreroZ(gohan);
		tablero.ubicarGuerreroZ(piccolo);
		
		assertEquals(false,goku.puedeTransformar());
		thrown.expect(ExcepcionNoPuedeEvolucionar.class);
		goku.transformar();
		
		goku.agregarKi(30);
		assertEquals(true,goku.puedeTransformar());
		
		assertEquals(false,piccolo.puedeTransformar());
		thrown.expect(ExcepcionNoPuedeEvolucionar.class);
		piccolo.transformar();
		
		piccolo.agregarKi(10);
		assertEquals(false,piccolo.puedeTransformar());
		thrown.expect(ExcepcionNoPuedeEvolucionar.class);
		piccolo.transformar();
		
		piccolo.agregarKi(10);
		assertEquals(true,piccolo.puedeTransformar());
		}
	
	
	@Test
	public void test04EvolucionarYChequearQueSeMueveCorrectamente(){
	
		Tablero tablero = new Tablero();
		Goku goku = new Goku();
		Gohan gohan = new Gohan();
		Piccolo piccolo = new Piccolo();
		tablero.ubicarGuerreroZ(goku);
		tablero.ubicarGuerreroZ(gohan);
		tablero.ubicarGuerreroZ(piccolo);
		tablero.mover(goku, "Abajo");
		assertEquals(0, tablero.obtenerColumnaDe(goku));
		assertEquals(2, tablero.obtenerFilaDe(goku));
		
		goku.agregarKi(30);
		goku.transformar();
		
		tablero.mover(goku, "Abajo");
		assertEquals(0, tablero.obtenerColumnaDe(goku));
		assertEquals(5, tablero.obtenerFilaDe(goku));
		
		tablero.mover(goku, "Derecha");
		assertEquals(3, tablero.obtenerColumnaDe(goku));
		assertEquals(5, tablero.obtenerFilaDe(goku));
		
		piccolo.agregarKi(25);
		tablero.mover(piccolo, "Derecha");
		assertEquals(4, tablero.obtenerColumnaDe(piccolo));
		assertEquals(0, tablero.obtenerFilaDe(piccolo));
		
		piccolo.transformar();
		tablero.mover(piccolo, "Derecha");
		assertEquals(7, tablero.obtenerColumnaDe(piccolo));
		assertEquals(0, tablero.obtenerFilaDe(piccolo));
		tablero.mover(piccolo, "Abajo");
		assertEquals(7, tablero.obtenerColumnaDe(piccolo));
		assertEquals(3, tablero.obtenerFilaDe(piccolo));
		
		tablero.mover(piccolo, "Arriba");
		assertEquals(7, tablero.obtenerColumnaDe(piccolo));
		assertEquals(0, tablero.obtenerFilaDe(piccolo));
		
	}
	
	@Test
	public void test05CrearUnJuegoCon2JugadoresYSus3Personajes(){
		Juego juego = new Juego();

		assertEquals(0, juego.obtenerFilaDePersonaje1JugadorGuerreroZ());
		assertEquals(0, juego.obtenerColumnaDePersonaje1JugadorGuerreroZ());
		
		assertEquals(0, juego.obtenerFilaDePersonaje2JugadorGuerreroZ());
		assertEquals(1, juego.obtenerColumnaDePersonaje2JugadorGuerreroZ());
		
		assertEquals(0, juego.obtenerFilaDePersonaje3JugadorGuerreroZ());
		assertEquals(2, juego.obtenerColumnaDePersonaje3JugadorGuerreroZ());
		
		assertEquals(9, juego.obtenerFilaDePersonaje1JugadorEnemigoDeLaTierra());
		assertEquals(7, juego.obtenerColumnaDePersonaje1JugadorEnemigoDeLaTierra());
		
		assertEquals(9, juego.obtenerFilaDePersonaje2JugadorEnemigoDeLaTierra());
		assertEquals(8, juego.obtenerColumnaDePersonaje2JugadorEnemigoDeLaTierra());
		
		assertEquals(9, juego.obtenerFilaDePersonaje3JugadorEnemigoDeLaTierra());
		assertEquals(9, juego.obtenerColumnaDePersonaje3JugadorEnemigoDeLaTierra());
		
	}
	@Test
	public void test06UbicarPersonajesYAtacarConDistanciaSuficiente(){
		Tablero tablero = new Tablero();
		Goku goku = new Goku();
		Freezer freezer = new Freezer();	
		tablero.ubicarEnTablero(5, 5, goku);
		tablero.ubicarEnTablero(6, 5, freezer);
		goku.atacaA(freezer, tablero);
		
		assertEquals(380, freezer.puntosDeVida());
		
	}
	
	@Test
	public void test07UbicarPersonajesYAtacarConDistanciaInsuficiente(){
		Tablero tablero = new Tablero();
		Goku goku = new Goku();
		Freezer freezer = new Freezer();	
		tablero.ubicarEnTablero(1, 5, goku);
		tablero.ubicarEnTablero(6, 5, freezer);

		thrown.expect(ExcepcionNoSePuedeRealizarAtaque.class);
		goku.atacaA(freezer, tablero);
		

		assertEquals(400, freezer.puntosDeVida());
		
	}
	
}


